const express = require("express");
const app = express();
const PORT = 8080;
app.use(express.json());

const products = require("./data.json");

app.get("/products", (req, res) => {
  res.status(200).send(products);
});


app.listen(PORT, () => console.log(`API RUNNING AT PORT ${PORT}`));
